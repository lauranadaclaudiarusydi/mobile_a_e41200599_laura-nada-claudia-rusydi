class Guru {
  int? id;
  String? nama;
  String? departemen;

  Guru.id(this.id);
  Guru.nama(this.nama);
  Guru.departemen(this.departemen);
}
