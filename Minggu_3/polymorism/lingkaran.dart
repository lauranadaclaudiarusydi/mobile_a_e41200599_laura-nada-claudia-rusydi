import 'bangun_datar.dart';

class Lingkaran extends Bangun_datar {
  double? jari_jari = 0.0;
  double? pi = 0.0;

  Lingkaran(double jari_jari, double pi) {
    this.jari_jari = jari_jari;
    this.pi = pi;
  }

  @override
  double luas() {
    return pi! * jari_jari! * jari_jari!;
  }

  @override
  double keliling() {
    return 2 * pi! * jari_jari!;
  }
}
